export enum EIconButtonSizes {
    small = 'small',
    medium = 'medium',
    large = 'large',
}
