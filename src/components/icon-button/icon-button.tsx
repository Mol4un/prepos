import React from 'react';
import cn from 'classnames';
import {EIconButtonSizes} from './model'
import style from './style.css';

interface IconButtonProps {
    icon: 'search' | 'filter-open' | 'filter-close'
    size: EIconButtonSizes;
    onClick?: () => void;
}

const IconButton: React.FC<IconButtonProps> = ({size, icon, children, onClick }) => (
    <button className={cn(style.main, style[size], style[icon])}>{children}</button>
)

export default IconButton
