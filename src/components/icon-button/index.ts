import IconButton  from './icon-button';
import { EIconButtonSizes } from './model';

export default IconButton;

export {
    IconButton,
    EIconButtonSizes
};
