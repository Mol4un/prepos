import React from 'react';
import styles from './styles.css';
import { Card } from '../../components';
import { CardItem } from "../../containers/main-detail-item-page/main-detail-item-page";
import {FormRow} from "../../components/form-row/form-row";

/*interface AppProps {
    items: Array<Item>;
}*/
interface DetailProps {
    item: CardItem
}

class DetailForm extends React.Component<DetailProps, {}> {


    render() {
        return (
            <main className={styles.wrapper}>
                <Card src={this.props.item.image_url} alt={this.props.item.name} />
                <form>
                    <FormRow name={'Name'} value={this.props.item.name} />
                    <FormRow name={'Tagline'} value={this.props.item.tagline} />
                    <FormRow name={'First Brewed'} value={this.props.item.first_brewed} />
                    <FormRow name={'Description'} value={this.props.item.description} />
                    <FormRow name={'ABV'} value={this.props.item.abv} />
                    <FormRow name={'IBU'} value={this.props.item.ibu} />
                    <FormRow name={'Target fg'} value={this.props.item.target_fg} />
                    <FormRow name={'Target og'} value={this.props.item.target_og} />
                    <FormRow name={'EBC'} value={this.props.item.ebc} />
                    <FormRow name={'SRM'} value={this.props.item.srm} />
                    <FormRow name={'PH'} value={this.props.item.ph} />
                    <FormRow name={'Attenuation level'} value={this.props.item.attenuation_level} />
                    <FormRow name={'Volume (value)'} value={this.props.item.volume.value} />
                    <FormRow name={'Volume (unit)'} value={this.props.item.volume.unit} />
                    <FormRow name={'Boil volume (value)'} value={this.props.item.boil_volume.value} />
                    <FormRow name={'Boil volume (unit)'} value={this.props.item.boil_volume.unit} />
                    <FormRow name={'Brewer tips'} value={this.props.item.brewers_tips} />
                    <FormRow name={'Contributed by'} value={this.props.item.contributed_by} />
                </form>
            </main>
        )
    }
}

export default DetailForm;
