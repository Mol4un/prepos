import React from 'react';
import cn from 'classnames';

import style from './styles.css';

interface CardProps {
    onClick?: () => void;
    src: string;
    alt: string;
    description?: string;
}

const Card: React.FC<CardProps> = ({
                                       src,
                                       alt,
                                       onClick,
                                       description
                                   }) => (
    <div className={cn(style.main, style.size, (description && style.grid))}>
        <img
            className={cn(style.img)}
            src={src}
            alt={alt}
        />
        <div className={cn(style.descriptionLabelContainer)}>
            <p className={cn(style.p)}>{description}</p>
        </div>
    </div>
)

export default Card;
