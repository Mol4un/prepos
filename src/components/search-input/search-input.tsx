import React, {HTMLAttributes} from 'react';
import style from './styles.css';
import cn from 'classnames';
import {EIconButtonSizes, IconButton} from '../../components';

interface SearchInputProps extends Omit<HTMLAttributes<HTMLInputElement>, 'id' | 'name'> {
    id: string | number;
    placeholder: string;
    size?: EIconButtonSizes;
    inputRef?: React.RefObject<HTMLInputElement>;
}

export const SearchInput: React.FC<SearchInputProps> = ({
                                                            inputRef,
                                                            placeholder,
                                                            id,
                                                            size = EIconButtonSizes.medium,
                                                            ...rest }) => (
    <>
        <input className={cn(style.searchField, style[size])} id={String(id)} ref={inputRef} placeholder={placeholder} {...rest}/>
        <IconButton size={size} icon="search" onClick={() => {}}/>
    </>
)
