import React, {ComponentState} from 'react';
import cn from 'classnames';
import style from './style.css';

const options = [{
        label: "RU",
        value: "ru"
    }, {
        label: "EN",
        value: "en"
    }
];

interface I18Props {
    handleLocaleChange?: (locale: string) => {}
}

interface I18State {
    locale: 'en'
}

class I18Selector extends React.Component<I18Props, I18State> {

    constructor(props) {
        super(props);

        this.state = {
            locale: "en"
        };

        // Эта привязка обязательна для работы `this` в колбэке.
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({ locale: e.target.value });
    }

    render() {
        return (
                <div className={style.wrapper}>
                    <select className={style.selector} value={this.state.locale} onChange={this.handleChange}>
                        {options.map((option) => (
                            <option key={option.value} value={option.value}>{option.label}</option>
                        ))}
                    </select>
                </div>
        );
    }
}

export default I18Selector
