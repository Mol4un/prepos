import React, {HTMLAttributes} from 'react';
import style from './style.css';
import cn from 'classnames';

interface FormRowProps {
    name: string;
    value: string | number ;
}

export const FormRow: React.FC<FormRowProps> = ({name, value }) => (
    <p className={cn(style.wrapper)}>
        <label className={cn(style.labelName)}>{name}:</label>
        <label className={cn(style.labelValue)}>{value}</label>
    </p>
)
