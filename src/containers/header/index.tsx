import React from 'react';
import styles from './styles.css';

import SearchInput from '../../components/search-input/index';
import {EIconButtonSizes, I18Selector, IconButton} from "../../components";
import {Logo} from '../../assets';


console.log(styles);

class Header extends React.Component {
    firstInputRef = React.createRef<HTMLInputElement>()

    componentDidMount() {
        this.firstInputRef.current.focus();
    }

    render() {
        return (
            <header className={styles.wrapper}>
                <img src={Logo} className={styles.logo}/>
                <div className={styles.searchArea}>
                    <SearchInput inputRef={this.firstInputRef} placeholder={'Enter search text'} id="search-input-id"></SearchInput>
                    <IconButton icon="filter-open" size={EIconButtonSizes.medium} onClick={()=>{}}></IconButton>
                    <I18Selector></I18Selector>
                </div>
            </header>
        )
    }
}

export default Header
