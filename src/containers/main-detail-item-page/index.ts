import { lazy } from 'react'

export default lazy(() => import('./main-detail-item-page'))
