import React from 'react';
import styles from './styles.css';

import Header from "../../containers/header";
import { DetailForm } from "../../components";

console.log(styles);

export interface CardItem {
    "id": number;
    "name": string; //"Punk IPA 2007 - 2010",
    "tagline": string; //"Post Modern Classic. Spiky. Tropical. Hoppy.",
    "first_brewed": string; //"04/2007",
    "description": string; //"Our flagship beer that kick started the craft beer revolution. This is James and Martin's original take on an American IPA, subverted with punchy New Zealand hops. Layered with new world hops to create an all-out riot of grapefruit, pineapple and lychee before a spiky, mouth-puckering bitter finish.",
    "image_url": string; //"https://images.punkapi.com/v2/192.png",
    "abv": number; //6.0,
    "ibu": number; //60.0,
    "target_fg": number; //1010.0,
    "target_og": number; //1056.0,
    "ebc": number; //17.0,
    "srm": number; //8.5,
    "ph": number; //4.4,
    "attenuation_level": number; //82.14,
    "volume": {
        "value": number ; //20,
        "unit": string; //"liters"
    },
    "boil_volume": {
        "value": number; //25,
        "unit": string; //"liters"
    },
    "method": {
        "mash_temp": [
            {
                "temp": {
                    "value": number; //65,
                    "unit": string; //"celsius"
                },
                "duration": number; //75
            }
        ],
        "fermentation": {
            "temp": {
                "value": number; //19.0,
                "unit": string; //"celsius"
            }
        },
        "twist": any;
    },
    "ingredients": {
        "malt": [
            {
                "name": string; //"Extra Pale",
                "amount": {
                    "value": number; //5.3,
                    "unit": string; //"kilograms"
                }
            }
        ],
        "hops": [
            {
                "name": string; //"Ahtanum",
                "amount": {
                    "value": number; //17.5,
                    "unit": string; //"grams"
                },
                "add": string; //"start",
                "attribute": string; //"bitter"
            },
            {
                "name": string; //"Chinook",
                "amount": {
                    "value": number; //15,
                    "unit": string; //"grams"
                },
                "add": string; //"start",
                "attribute": string; //"bitter"
            }
        ]
        "yeast": string; //"Wyeast 1056 - American Ale™"
    },
    "food_pairing": Array<String>;/*[
        "Spicy carne asada with a pico de gallo sauce",
        "Shredded chicken tacos with a mango chilli lime salsa",
        "Cheesecake with a passion fruit swirl sauce"
    ]*/
    "brewers_tips": string; //"While it may surprise you, this version of Punk IPA isn't dry hopped but still packs a punch! To make the best of the aroma hops make sure they are fully submerged and add them just before knock out for an intense hop hit.",
    "contributed_by": string;//"Sam Mason <samjbmason>"
}
interface MainDetailItemProps {
    items: Array<CardItem>;
}
class MainDetailItemPage extends React.Component<MainDetailItemProps, {}> {
    constructor(props) {
        super(props);
    }

/* */
    render() {
        return (
            <React.Fragment >
                <Header/>
                <DetailForm item={this.props.items[0]}/>
            </React.Fragment>
        )
    }
}

export default MainDetailItemPage
