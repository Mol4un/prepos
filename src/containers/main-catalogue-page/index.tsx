import React from 'react';
import styles from './styles.css';

import Header from "../../containers/header";
import MainCatalogue from "../main-catalogue";

console.log(styles);

class MainCataloguePage extends React.Component {
    render() {
        return (
            <React.Fragment >
                    <Header/>
                    <MainCatalogue/>
            </React.Fragment>
        )
    }
}

export default MainCataloguePage
