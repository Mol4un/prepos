import React from 'react';
import styles from './styles.css';
import cn from 'classnames';

import {Chrono} from "react-chrono";
import {CardItem} from "../main-detail-item-page/main-detail-item-page";
import {TimelineItemModel} from "react-chrono/dist/models/TimelineItemModel";

interface MainTimelineProps {
    items: Array<CardItem>
}

class MainTimeline extends React.Component<MainTimelineProps, {}> {

    timeLineItems: Array<TimelineItemModel>;

    constructor(props) {
        super(props);
        this.timeLineItems = this.props.items.map(item => {
            return {
                title: item.first_brewed,
                cardTitle: item.name,
                cardDetailedText: item.description,
                media: {
                    type: "IMAGE",
                    source: {
                        url: item.image_url
                    }
                }
            }
        })
    }


    render() {
        return (
            <main className={cn(styles.wrapper)}>
                <Chrono items={this.timeLineItems} mode="VERTICAL"/>
            </main>
        )
    }
}

export default MainTimeline
