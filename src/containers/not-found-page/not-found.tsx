import React from "react";
import styles from './style.css'
import {I404} from "../../assets";
import cn from 'classnames';

class NotFound extends React.Component {
    render() {
        return (
            <div className={cn(styles.wrapper)}>
                <img src={I404} className={styles.logo}/>
            </div>
        )
    }
}

export default NotFound
