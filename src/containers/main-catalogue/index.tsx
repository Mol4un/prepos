import React from 'react';
import styles from './styles.css';
import { Card } from '../../components/index';
import {
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom"
import {URLs} from "../../__data__/urls";

class MainCatalogue extends React.Component {
    render() {
        const src = 'http://thebeeroness.com/wp-content/uploads/2013/03/Allagash-White.jpg';
        const alt = 'alt text'

        let data: Array<{key: string; src: string, alt: string, description: string}> = [];
        for(let i=1; i< 20; i++) {
            data.push({key: Date.now + '_' + i, alt, src, description: 'Some little description about this beer. Or mb i can add some functionality like floating popup.' });
        }

        return (
            <main className={styles.wrapper}>
                {data.map(function(d, idx){
                    return (
                       <Link  key={d.key}  to={URLs.detail.url} className={styles.link}>
                                <Card src={d.src}
                                          alt={d.alt}
                                          description={d.description}
                            />
                       </Link>
                    )
                })}
            </main>
        )
    }
}

export default MainCatalogue
