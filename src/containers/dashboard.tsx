import React, {Suspense} from 'react';

import {
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom"

import { URLs } from '../__data__/urls';
import ErrorBoundary from '../components/error-boundary';
import {item} from '../item';

import MainCataloguePage from "./main-catalogue-page";
import MainDetailItemPage from "./main-detail-item-page";
import MainTimelinePage from "./main-timeline-page";
import NotFound from "./not-found-page";



const Dashboard = () => (
    <Switch>
        <Route exact path="/">
            <Redirect to={URLs.catalogue.url} />
        </Route>
        <Route path={URLs.catalogue.url}>
            <MainCataloguePage/>
        </Route>
        <Route path={URLs.detail.url}>
            <ErrorBoundary>
                <Suspense fallback=".. loading">
                    <MainDetailItemPage items={[item, item, item, item]}/>
                </Suspense>
            </ErrorBoundary>
        </Route>
        <Route path={URLs.timeline.url}>
            <ErrorBoundary>
                <Suspense fallback=".. loading">
                    <MainTimelinePage items={[item, item, item, item]}/>
                </Suspense>
            </ErrorBoundary>
        </Route>
        <Route path="*">
            <ErrorBoundary>
                <Suspense fallback=".. loading">
                    <NotFound/>
                </Suspense>
            </ErrorBoundary>
        </Route>
    </Switch>
)

export default Dashboard;
