import React from 'react';
import styles from './styles.css';

import Header from "../../containers/header";
import MainTimeline from "../../containers/main-timeline";
import {CardItem} from "../../containers/main-detail-item-page/main-detail-item-page";

console.log(styles);

interface MainTimelinePageProps {
    items: Array<CardItem>;
}

class MainTimelinePage extends React.Component<MainTimelinePageProps, {}> {
    render() {
        return (
            <React.Fragment >
                <Header/>
                <MainTimeline items={this.props.items}/>
            </React.Fragment>
        )
    }
}

export default MainTimelinePage
