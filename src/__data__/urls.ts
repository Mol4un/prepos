import { getNavigations, getNavigationsValue } from '@ijl/cli';

console.log(getNavigationsValue('cards'))

const navigations = getNavigations('cards');

export const baseUrl = navigations['cards'];

export const URLs = {
    catalogue: {
        url: navigations['link.cards.main'],
    },
    detail: {
        url: navigations['link.cards.detail'],
    },
    timeline: {
        url: navigations['link.cards.timeline']
    }
}
