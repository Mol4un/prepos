import React from 'react';
import {BrowserRouter} from 'react-router-dom'
import Dashboard from './containers/dashboard'
import './app.css'
import {item} from "./item";
import MainTimelinePage from "./containers/main-timeline-page";
import name from 'package.json';
import {baseUrl} from "./__data__/urls";

const App = () => (
    // <MainCataloguePage/>
    // <MainDetailItem items={[item]}/>
    // <MainTimelinePage items={[item, item, item, item]}/>
     <BrowserRouter basename={baseUrl}>
        <Dashboard/>
     </BrowserRouter>
)

export default App
