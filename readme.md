# Beers map-catalogue

## MVP1
- Beers catalogue page 
- Beer detail page
- Beers timeline page

## Install

npm install

## Run&Build

npm start
npm run-script build:prod

#### Project links
- [git (bitbacket) repository](https://Mol4un@bitbucket.org/Mol4un/prepos/src/master/)
- [trello](https://trello.com/b/XG0Lt28v/stc-33-js)
- [jenkins](http://89.223.92.18:8080/view/build/job/stc-33-khafizov-airat/)
- [project](http://89.223.91.151:8080/cards)
- [punkapi](https://punkapi.com/documentation/v2)
- [figma](https://www.figma.com/file/KtrP5Tz64owZsyVwqRYNz6/beers-map?node-id=0%3A1)

