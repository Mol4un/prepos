declare module '*.css' {
    interface Style {
        [key: string]: string;
    }
    const style: Style;

    export default style
}

declare module '*.jpg' {
    const path: string;
    export default  path;
}

declare module '*.json' {
    const value: any;
    export default  value;
}
